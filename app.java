public class app {
    public static void main(String[] args) {
        Bike newBike = new Bike();
        System.out.println("Create new Bike");
        System.out.println("sound: " + newBike.getSound());
        System.out.println("lubrication: " + newBike.getLubrication());

        newBike.accelerate();
        System.out.println("lubrication: " + newBike.getLubrication());

        Car newCar = new Car();
        System.out.println("Create new Car");
        System.out.println("sound: " + newCar.getSound());
        System.out.println("tank: " + newCar.getTank());

        newCar.accelerate();
        System.out.println("tank: " + newCar.getTank());
    }
}