public class Car extends Vehicle{
    String tank;

    public Car() {
        this.sound = "vrooom";
        this.tank = "full";
    }

    public String getTank() {
        return tank;
    }

    @Override
    void accelerate() {
        this.tank = "empty";
    }
}
