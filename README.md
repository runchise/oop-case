# Case
## OOP question:
- I have a Bike class and a Car class, both are Vehicles.
- A Vehicle has the property `sound`.
- A Vehicle has the method `accelerate`.
- When a Vehicle accelerates, it will print the value of `sound`.
### Requirement for Bike:
1. A Bike has property `lubrication` with initial value "normal".
2. A Bike's `sound` is "swoosh".
3. When `accelerate` is called on a Bike, the `lubrication` will become "needs
maintenance".
### Requirement for Car:
1. A Car has the property `tank` with the initial value "full".
2. A Car's `sound` is "vrooom".
3. When `accelerate` is called on a car, the `tank` will become "empty".
# How to Run
1. compile `javac app.java`
2. run `java app`