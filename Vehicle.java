public abstract class Vehicle {
    String sound;
    abstract void accelerate();

    public String getSound() {
        return sound;
    }
}
