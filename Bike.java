public class Bike extends Vehicle {
    String lubrication;

    public Bike() {
        this.sound = "swoosh";
        this.lubrication = "normal";
    }

    public String getLubrication() {
        return lubrication;
    }

    @Override
    void accelerate() {
        this.lubrication = "needs maintenance";
    }
}
